var express = require("express");
var bodyParser = require("body-parser");
var expressValidator = require('express-validator');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

var routes = require("./routes/routes")(app);

var server = app.listen(3000, () => console.log("Listening on port %s...", server.address().port));

module.exports = app; // for testing