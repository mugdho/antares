Antares Quote Processing System
===

# Requirements and Thought Process

All the requirements have been captured in this app including the ones for extra points (except the geo locating one, which I couldn't get to today).


## Missing
These are a few things I would have liked to do, but couldn't get to.

* A better authentication system and intercepts all requests without needing to add code as its done in the quotes controller.
* Usage of relationships in Bookshelf objects. I had kept that for the end, and didnt get to that either.
* Making the quotes a more extensible for the future and having the line items in a different table.
* Securing all the methods where login should be required. I could just get to the accept / reject / update.
* Having the address in a different table and using it for geo locating.
* More elegant way for snake case -> camel case conversions
* An accompanying API document from Apiary or Swagger. I started making one, but sorta put it aside till end.
* Well defined roles for CONSUMER, CONTRACTOR, ADMIN etc.
* Would have liked to write some more test cases.
* Lastly would have liked to deploy a version to heroku for quicker view and not needing the local setup.


## Quotes Expiry

I think there are a few ways to indicate expiry of the quote.
* We could use a job that checks every so often if there are expired quotes and act accordingly.
* We could store the quote information in redis with an expiry of 5 mins. We could hook the cache evacuation/expiry event to the app which triggers an update to the required groups. I am more drawn to something like this since its an event model as opposed to a polling model.


# Setup

## Requirements
* Postgres
* node and npm

### Postgres

```
CREATE user antares password 'antares';
CREATE database antares owner antares;
CREATE database antares_test owner antares;
```

### Command line

```
npm install
npm install -g knex
knex migrate:latest
```

### Running

```
npm run dev
```