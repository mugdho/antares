create table antares_user (
    id serial primary key,
    name varchar(255),
    email varchar(255),
    password varchar(100),
    address_line1 varchar(255),
    address_line2 varchar(255),
    salt varchar(20),
    city varchar(255),
    state char(2),
    country char(2),
    zip char(5),
    type char(40)
);

create table quote (
    id serial primary key,
    consumer_id integer,
    contractor_id integer,
    line_item1 decimal(5,2),
    line_item2 decimal(5,2),
    etc decimal(5,2),
    total decimal(5,2),
    request_time timestamp,
    action_time timestamp,
    status varchar(10)
);

create table token (
    id serial primary key,
    user_id integer,
    token varchar(255),
    expires timestamp
);