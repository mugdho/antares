module.exports = {
    development: {
        client: 'pg',
        connection: {
            host     : 'localhost',
            port     : 5432,
            user     : 'antares',
            password : 'antares',
            database : 'antares_test'
        },
        migrations: {
            tableName: 'knex_migrations'
        }    
    }
};
