var config = require("config");
var dbConfig = config.get("database");

var knex = require('knex')({
    client: 'pg',
    connection: {
        host     : dbConfig.host,
        port     : dbConfig.port,    
        user     : dbConfig.user,
        password : dbConfig.password,
        database : dbConfig.db
    }
});

var bookshelf = require('bookshelf')(knex);

var User = bookshelf.Model.extend({
    tableName: 'antares_user'
});

module.exports = User;