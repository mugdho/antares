var User = require("../models/user");
var cryptoUtils = require('../utils/crypto_utils');
var randomstring = require("randomstring");
var Token = require("../models/token");

var LoginController = function() {}

LoginController.prototype.index = (req, res) => {
    let params = req.body;
    let model = new User({email: params.email});

    model.on('fetched', user => {
        if (user) {
            let password = cryptoUtils.hashPassword(params.password, model.get('salt'));
            // TODO Move this to JWT
            if (password == model.get('password')) {
                let tokenValue = randomstring.generate({length: 20, charset: 'alphabetic'});
                new Token({
                    user_id: user.get('id'),
                    token: tokenValue,
                    expires: new Date(new Date().setDate(new Date().getDate() + 1))
                }).save().then(token => {
                    res.send({
                        name: user.get('name'),
                        id: token.get('id'),
                        token: token.get('token'),
                        expires: token.get('expires')
                    });
                });
            } else {
                res.sendStatus(401);
            }
        } else {
            res.sendStatus(401);
        }
    });

    model.fetch();
}

module.exports = new LoginController();