var User = require('../models/user');
var randomstring = require("randomstring");
var CryptoUtils = require("../utils/crypto_utils");

var UsersController = function() {
}

UsersController.prototype.create = (req, res) => {
    let data = req.body;
    let salt = randomstring.generate({length: 20, charset: 'alphabetic'});
    let password = CryptoUtils.hashPassword(data.password, salt);

    req.checkBody('email', 'Invalid Email').isEmail();

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            res.status(400).send('There have been validation errors: ' + result.array());
            return;
        } else {
            let user = {
                    name: data.name,
                    email: data.email,
                    password: password,
                    salt: salt,
                    type: data.type,
                    address_line1: data.address.line1, // TODO Convert this to a better representation
                    address_line2: data.address.line2,
                    address_city: data.address.city,
                    address_state: data.address.state,
                    address_zip: data.address.zip,
            }

            // TODO Move this call to a common service
            new User({email: data.email}).fetch().then((model) => {
                if (model) {
                    res.sendStatus(409);
                } else {
                    new User(user).save().then((model) => {
                        let user = {
                            "id": model.get("id"),
                            "name": model.get("name"),
                            "email": model.get("email"),
                            "type": model.get("type"),
                            "address": {
                                "line1": model.get("address_line1"),
                                "line2": model.get("address_line2"),
                                "city": model.get("address_city"),
                                "state": model.get("address_state"),
                                "country": model.get("address_country"),
                                "zip": model.get("address_zip")
                            }
                        }
                        res.send(data);
                    });
                }
            });
        }
    });
}

module.exports = new UsersController();