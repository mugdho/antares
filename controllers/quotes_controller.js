var Quote = require('../models/quote');
var User = require('../models/user');
var mailService = require('../services/mail_service');
var Token = require("../models/token");

var QuotesController = function() {
}

QuotesController.prototype.create = (req, res) => {
    let data = req.body;

    let consumer = new User({name: data.consumerName});
    consumer.fetch();

    consumer.on('fetched', (consumer) => {
        // TODO move this to getting contractor from token
        new User({email: data.email}).fetch().then((contractor) => {
            let quote = {
                consumer_id: consumer.get("id"),
                contractor_id: contractor.get("id"),
                line_item1: data.lineItem1,
                line_item2: data.lineItem2,
                etc: data.etc,
                total: data.total,
                request_time: new Date(),
                status: 'INIT'
            }

            new Quote(quote).save().then((model) => {
                mailService.send(contractor.get("email"), consumer.get("email"), "Quote Submitted", "Total Quote Value: " + model.get("total"));
                res.send({
                    id: model.get("id"),
                    consumerName: consumer.get("name"),
                    email: contractor.get("email"),
                    lineItem1: model.get("line_item1"),
                    lineItem2: model.get("line_item2"),
                    etc: model.get("etc"),
                    total: model.get("total")
                });
            });
        })
    });
}

QuotesController.prototype.update = (req, res) => {
    let quoteId = req.params.quoteId;
    let bearer = req.headers["authorization"];

    // TODO Use a security framework for this.
    if (!bearer || bearer.split(" ").length != 2) {
        res.sendStatus(401);
    } else {
        let tokenValue = bearer.split(" ")[1];
        new Token({token: tokenValue}).fetch().then((token) => {
            if (token.get('expires') < new Date()) {
                res.sendStatus(401);
            } else {

                let quotePromise = new Quote({id: quoteId}).fetch();
                let userPromise = new User({id: token.get('user_id')}).fetch();

                Promise.all([quotePromise, userPromise]).then(values => {
                    let quote = values[0];
                    let user = values[1];
                    let data = {}

                    if (quote) {
                        if (user.get('type').trim() === "CONSUMER") {
                            if (quote.get('consumer_id') == user.get('id')) {
                                data.status = req.body.status;
                                data.action_time = new Date();
                            } else {
                                res.sendStatus(401);
                                return;
                            }
                        } else if (user.get('type').trim() === "CONTRACTOR") {
                            if (quote.get('contractor_id') == user.get('id')) {
                                for (var reqKey in req.body) {
                                    // CamelCase to SnakeCase. Again need a more elegant way for this.
                                    let key = reqKey.replace(/\.?([A-Z])/g, function (x,y){return "_" + y.toLowerCase()}).replace(/^_/, "")
                                    if (key != "status" && key != "action_time") {
                                        data[key] = req.body[reqKey];
                                    }
                                }
                            } else {
                                res.sendStatus(401);
                                return;
                            }
                        }

                        quote.save(data).then((model) => {
                            res.send({
                                id: model.get("id"),
                                lineItem1: model.get("line_item1"),
                                lineItem2: model.get("line_item2"),
                                etc: model.get("etc"),
                                total: model.get("total"),
                                status: model.get("status")                    
                            });
                        });
                    } else {
                        res.sendStatus(404);
                    }
                })
            }
        })
    }
}

QuotesController.prototype.accept = (req, res) => {
    let quoteId = req.params.quoteId;
    let quote = new Quote({id: quoteId});
    quote.fetch()
    quote.on('fetched', (quote) => {
        if (quote) {
            quote.status = 'ACCEPTED';
            quote.accept_time = new Date();

            quote.save({status: quote.status, accept_time: quote.accept_time}).then((model) => {
                res.send({
                    id: model.get("id"),
                    lineItem1: model.get("line_item1"),
                    lineItem2: model.get("line_item2"),
                    etc: model.get("etc"),
                    total: model.get("total"),
                    status: model.get("status")                    
                });
            });
        } else {
            res.sendStatus(404);
        }
    });
}

QuotesController.prototype.get = (req, res) => {
    let quoteId = req.params.quoteId;
    new Quote({id: quoteId}).fetch().then((quote) => {
        // TODO Add contractor details
        if (quote) {
            res.send({
                id: quote.get("id"),
                lineItem1: quote.get("line_item1"),
                lineItem2: quote.get("line_item2"),
                etc: quote.get("etc"),
                total: quote.get("total"),
                status: quote.get("status")
            });
        } else {
            res.sendStatus(404);
        }
    })
}

module.exports = new QuotesController();