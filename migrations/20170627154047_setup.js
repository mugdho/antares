
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('antares_user', function(table){
            table.increments('id').unsigned().primary();
            table.string('password', 100);
            table.string('name', 255);
            table.string('email', 255);
            table.string('salt', 20);
            table.string('address_line1', 255);
            table.string('address_line2', 255);
            table.string('address_city', 255);
            table.string('address_state', 2);      
            table.string('address_zip', 9);
            table.string('address_country', 2);
            table.string('type', 255);
            table.timestamps();
        }),
        knex.schema.createTable('quote', function(table){
            table.increments('id').unsigned().primary();
            table.integer('consumer_id').index().references('id').inTable('antares_user');
            table.integer('contractor_id').index().references('id').inTable('antares_user');
            table.decimal('line_item1', 5, 2);
            table.decimal('line_item2', 5, 2);
            table.decimal('etc', 5, 2);
            table.decimal('total', 5, 2);
            table.dateTime('request_time', 2);      
            table.dateTime('action_time', 9);
            table.string('status', 10);
            table.timestamps();
        }),
        knex.schema.createTable('token', function(table){
            table.increments('id').unsigned().primary();
            table.integer('user_id').index().references('id').inTable('antares_user');
            table.string('token', 255).notNull();
            table.string('salt', 20);
            table.dateTime('expires');
            table.timestamps();
        })        
  ])  
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('token'),
        knex.schema.dropTable('quote'),
        knex.schema.dropTable('antares_user')
    ])
};
