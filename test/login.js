let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let User = require('../models/user');

chai.use(chaiHttp);

describe('Working with Login', () => {
    after((done) => {
        new User().fetchAll().then(models => { 
            let promises = models.map(model => model.destroy());
            Promise.all(promises).then(_ => done());
        });
    });
    it('it should allow a contractor with correct credentials to login', (done) => {
            done()
    });
    it('it should throw an contractor if there are credential mismatches', (done) => {
            done()
    });    
    it('it provide a valid token with the response to the contractor', (done) => {
            done()
    });    
    it('it should allow a consumer with correct credentials to login', (done) => {
            done()
    });
    it('it should throw an consumer if there are credential mismatches', (done) => {
            done()
    });    
    it('it provide a valid token with the response to the consumer', (done) => {
            done()
    });
});