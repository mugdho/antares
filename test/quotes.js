let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var Quote = require('../models/quote');
var User = require('../models/user');
var Token = require('../models/token');

chai.use(chaiHttp);

describe('Working with Quotes', () => {
    var consumerToken = "consumer";
    var consumerToken2 = "consumer2";
    contractorToken = "contractor";
    before((done) => {
        let consumer = new User({
            name: "Luke Skywalker",
            email: "luke@tol.com",
            password: "pass12",
            salt: "salt",
            type: "CONSUMER",
            address_line1: "18 Desert View Rd",
            address_line2: "",
            address_city: "Tatooine City",
            address_state: "TT",
            address_country: "EM",
            address_zip: "12345"
        }).save();
        let contractor = new User({
            name: "Han Solo",
            email: "16parsecs@solo.com",
            password: "pass12",
            salt: "salt",
            type: "CONTRACTOR",
            address_line1: "120 Millenium Way",
            address_line2: "",
            address_city: "Bespin City",
            address_state: "CC",
            address_country: "EM",
            address_zip: "71838"
        }).save();
        let consumer2 = new User({
            name: "Boba Fett",
            email: "fettman@boutines.com",
            password: "pass12",
            salt: "salt",
            type: "CONSUMER",
            address_line1: "None of ya",
            address_line2: "",
            address_city: "Business",
            address_state: "NA",
            address_country: "NA",
            address_zip: "00000"
        }).save();

        Promise.all([consumer, contractor, consumer2]).then(values => {
            let consumerTokenPromise = new Token({
                user_id: values[0].get('id'),
                token: consumerToken,
                expires: new Date(new Date().setDate(new Date().getDate() + 1)) 
            }).save();
            let contractorPromise = new Token({
                user_id: values[1].get('id'),
                token: contractorToken,
                expires: new Date(new Date().setDate(new Date().getDate() + 1)) 
            }).save();
            let consumer2Promise = new Token({
                user_id: values[2].get('id'),
                token: consumerToken2,
                expires: new Date(new Date().setDate(new Date().getDate() + 1)) 
            }).save();

            Promise.all([consumerTokenPromise, contractorPromise, consumer2Promise]).then(_ => {
                done();
            });
        })
    });

    beforeEach((done) => {
        let promise = new Quote().fetchAll().then((models) => { 
           models.map(model => model.destroy());
        });
        Promise.all([promise]).then(_ => {
            done();
        });
    });

    after((done) => {
        let tokenPromise = new Token().fetchAll().then(models => { 
            models.map(model => model.destroy());
            new Quote().fetchAll().then((models) => { 
                models.map(model => model.destroy());
               new User().fetchAll().then(models => { 
                    models.map(model => model.destroy());
                    done();
                });
            });
        });
    });

    describe('Quotes', () => {
        it('it should create a new quote', (done) => {
            chai.request(server)
                .post('/quotes')
                .type('form')
                .send({
                    "consumerName": "Luke Skywalker",
                    "email": "16parsecs@solo.com",
                    "lineItem1": 134.24,
                    "lineItem2": 643.19,
                    "etc": 10.1,
                    "total": 807.53
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it('it should show the quote', (done) => {
            chai.request(server)
                .post('/quotes')
                .set("Authorization", "Bearer " + contractorToken)
                .type('form')
                .send({
                    "consumerName": "Luke Skywalker",
                    "email": "16parsecs@solo.com",
                    "lineItem1": 134.24,
                    "lineItem2": 643.19,
                    "etc": 10.1,
                    "total": 807.53
                })
                .end((err, res) => {
                    let quoteId = res.body.id;
                    chai.request(server)
                        .get('/quotes/' + quoteId)
                        .set("Authorization", "Bearer " + contractorToken)
                        .send()
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property("status").eql("INIT");
                            done();
                        });
                });
        });

        it('it should let the consumer accept the quote', (done) => {
            chai.request(server)
                .post('/quotes')
                .set("Authorization", "Bearer " + consumerToken)
                .type('form')
                .send({
                    "consumerName": "Luke Skywalker",
                    "email": "16parsecs@solo.com",
                    "lineItem1": 134.24,
                    "lineItem2": 643.19,
                    "etc": 10.1,
                    "total": 807.53
                })
                .end((err, res) => {
                    let quoteId = res.body.id;
                    chai.request(server)
                        .put('/quotes/' + quoteId)
                        .set("Authorization", "Bearer " + consumerToken)
                        .send({
                            "status": "ACCEPTED"
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property("status").eql("ACCEPTED");
                            console.log("Here");
                            done();
                        });
                });
        });

        it('it should let the consumer reject the quote', (done) => {
            chai.request(server)
                .post('/quotes')
                .type('form')
                .send({
                    "consumerName": "Luke Skywalker",
                    "email": "16parsecs@solo.com",
                    "lineItem1": 134.24,
                    "lineItem2": 643.19,
                    "etc": 10.1,
                    "total": 807.53
                })
                .end((err, res) => {
                    let quoteId = res.body.id;
                    chai.request(server)
                        .put('/quotes/' + quoteId)
                        .set("Authorization", "Bearer " + consumerToken)
                        .send({
                            "status": "REJECTED"
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property("status").eql("REJECTED");
                            done();
                        });
                });
        });

        it('it should send an error if anyone not a consumer tries to accept the quote', (done) => {
            chai.request(server)
                .post('/quotes')
                .type('form')
                .send({
                    "consumerName": "Luke Skywalker",
                    "email": "16parsecs@solo.com",
                    "lineItem1": 134.24,
                    "lineItem2": 643.19,
                    "etc": 10.1,
                    "total": 807.53
                })
                .end((err, res) => {
                    let quoteId = res.body.id;
                    chai.request(server)
                        .put('/quotes/' + quoteId)
                        .set("Authorization", "Bearer " + consumerToken2)
                        .send({
                            "status": "REJECTED"
                        })
                        .end((err, res) => {
                            res.should.have.status(401);
                            done();
                        });
                });
        });

        it('it should a 404 error if the quote is not found', (done) => {
            chai.request(server)
                .get('/quotes/' + 1267152)
                .send()
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
            });
        });

        it('it should let the creator of the quote update it', (done) => {
            chai.request(server)
                .post('/quotes')
                .type('form')
                .send({
                    "consumerName": "Luke Skywalker",
                    "email": "16parsecs@solo.com",
                    "lineItem1": 134.24,
                    "lineItem2": 643.19,
                    "etc": 10.1,
                    "total": 807.53
                })
                .end((err, res) => {
                    let quoteId = res.body.id;
                    chai.request(server)
                        .put('/quotes/' + quoteId)
                        .set("Authorization", "Bearer " + contractorToken)
                        .send({
                            "lineItem1": 678.21
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property("lineItem1").eql(678.21);
                            done();
                        });
                });
        });

        it('it shouldnt let the anyone other than the creator of the quote update it', (done) => {
            done()
        });

        it('it should let the creator update the quote only if it has not been accepted or rejected', (done) => {
            done()
        });

        it('it should not let the consumer update the quote after expiry', (done) => {
            done()
        });

        it('it should not let the contractor update the quote after expiry', (done) => {
            done()
        });

        it('it should reject the quote if the consumer name is not present', (done) => {
            done()
        });

        it('it should check if emails are sent after a new quote is created', (done) => {
            // In the absence of an email service we could write to a common location and validate.
            done()
        });
        it('it should allow only a user of type contractor to submit a quote', (done) => {
            done()
        });
        it('it should allow only a user of type consumer to accept or reject a quote', (done) => {
            done()
        });        
        
    })
});