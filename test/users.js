let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let User = require('../models/user');

chai.use(chaiHttp);

describe('Working with Users', () => {
    after((done) => {
        new User().fetchAll().then(models => { 
            let promises = models.map(model => model.destroy());
            Promise.all(promises).then(_ => done());
        });
    });

    describe('Create User Profiles', () => {
        it('it should add a consumer profile', (done) => {
            chai.request(server)
                .post('/users')
                .type('form')
                .send({
                    'firstName': 'Luke',
                    'lastName': 'Skywalker',
                    'email': 'luke@tol.com',
                    'password': 'abc123',
                    'type': 'CONSUMER',
                    'address': {
                        'line1': '85 Round Street',
                        "city": "Desert View",
                        "state": "WY",
                        "country": "Tatooine",
                        "zip": "98366"
                    }
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it('it should add a contractor profile', (done) => {
            chai.request(server)
                .post('/users')
                .type('form')
                .send({
                    "name": "Han Solo",
                    "email": "16parsecs@solo.com",
                    "password": "pass12",
                    "type": "CONTRACTOR",
                    "address": {
                        "line1": "120 Millenium Way",
                        "line2": "",
                        "city": "Bespin",
                        "state": "CC",
                        "country": "EM",
                        "zip": "87623"
                    }
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
        it('it should throw a duplicate error status', (done) => {
            chai.request(server)
                .post('/users')
                .type('form')
                .send({
                    'firstName': 'Luke',
                    'lastName': 'Skywalker',
                    'email': 'luke@tol.com',
                    'password': 'abc123',
                    'type': 'CONSUMER',
                    'address': {
                        'line1': '85 Round Street',
                        "city": "Desert View",
                        "state": "WY",
                        "country": "Tatooine",
                        "zip": "98366"
                    }
                })
                .end((err, res) => {
                    res.should.have.status(409);
                    done();
                });
        });
        it('it should detect a duplicate email address', (done) => {
            chai.request(server)
                .post('/users')
                .type('form')
                .send({
                    'firstName': 'Luke',
                    'lastName': 'Skywalker',
                    'email': 'luke@tol.com',
                    'password': 'abc123',
                    'type': 'CONSUMER',
                    'address': {
                        'line1': '85 Round Street',
                        "city": "Desert View",
                        "state": "WY",
                        "country": "Tatooine",
                        "zip": "98366"
                    }
                })
                .end((err, res) => {
                    res.should.have.status(409);
                    done();
                });
        });
        it('it should throw an error for an invalid email address', (done) => {
            chai.request(server)
                .post('/users')
                .type('form')
                .send({
                    'name': 'Luke SKywalker',
                    'email': 'luketol.com',
                    'password': 'abc123',
                    'type': 'CONSUMER',
                    'address': {
                        'line1': '85 Round Street',
                        "city": "Desert View",
                        "state": "WY",
                        "country": "Tatooine",
                        "zip": "98366"
                    }
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it('it should throw an error if the email is empty', (done) => {
            done()
        });
        it('it should throw an error if the name is empty', (done) => {
            done()
        });
        it('it should throw an error if the type is not CONSUMER or CONTRACTOR', (done) => {
            done()
        });
        it('it should verify basic password requirements', (done) => {
            done()
        });
        it('it should not allow a user to update profile details', (done) => {
            done()
        });
        it('it should not allow a user to delete profile details', (done) => {
            done()
        });

    });
});