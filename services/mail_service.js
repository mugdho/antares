var config = require('config');
var MailService = function() {}

MailService.prototype.send = (from, to, subject, text) => {
    let key = config.mail.key;
    console.log("A quote has been submitted from " + from + " for " + to);
    console.log("Mail text: " + text);
}

module.exports = new MailService();