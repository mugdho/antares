const crypto = require('crypto');
var CryptoUtils = function() {}

CryptoUtils.prototype.hashPassword = (password, salt) => {
    const hash = crypto.createHmac('sha256', salt)
                   .update(password)
                   .digest('hex');

    return hash;
}

module.exports = new CryptoUtils();