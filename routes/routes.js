var users = require("../controllers/users_controller");
var quotes = require("../controllers/quotes_controller");
var login = require("../controllers/login_controller");

var appRouter = (app) => {

    app.post("/users", users.create);
    app.post("/login", login.index);
    // // app.post("/contractors/:contractorId/distance", users.create);
    app.post("/quotes", quotes.create);
    app.put("/quotes/:quoteId", quotes.update);
    app.get("/quotes/:quoteId", quotes.get);
}

module.exports = appRouter;